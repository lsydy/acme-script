#!/bin/bash
#
# Encrypted by Rangga Fajar Oktariansyah (Anak Gabut Thea)
#
# This file has been encrypted with BZip2 Shell Exec <https://github.com/FajarKim/bz2-shell>
# The filename '1acme.sh' encrypted at Sun Jun 23 06:22:08 UTC 2024
# I try invoking the compressed executable with the original name
# (for programs looking at their name).  We also try to retain the original
# file permissions on the compressed file.  For safety reasons, bzsh will
# not create setuid or setgid shell scripts.
#
# WARNING: the first line of this file must be either : or #!/bin/bash
# The : is required for some old versions of csh.
# On Ultrix, /bin/bash is too buggy, change the first line to: #!/bin/bash5
#
# Don't forget to follow me on <https://github.com/FajarKim>
skip=75

tab='	'
nl='
'
IFS=" $tab$nl"

# Make sure important variables exist if not already defined
# $USER is defined by login(1) which is not always executed (e.g. containers)
# POSIX: https://pubs.opengroup.org/onlinepubs/009695299/utilities/id.html
USER=${USER:-$(id -u -n)}
# $HOME is defined at the time of login, but it could be unset. If it is unset,
# a tilde by itself (~) will not be expanded to the current user's home directory.
# POSIX: https://pubs.opengroup.org/onlinepubs/009696899/basedefs/xbd_chap08.html#tag_08_03
HOME="${HOME:-$(getent passwd $USER 2>/dev/null | cut -d: -f6)}"
# macOS does not have getent, but this works even if $HOME is unset
HOME="${HOME:-$(eval echo ~$USER)}"
umask=`umask`
umask 77

bztmpdir=
trap 'res=$?
  test -n "$bztmpdir" && rm -fr "$bztmpdir"
  (exit $res); exit $res
' 0 1 2 3 5 10 13 15

case $TMPDIR in
  / | */tmp/) test -d "$TMPDIR" && test -w "$TMPDIR" && test -x "$TMPDIR" || TMPDIR=$HOME/.cache/; test -d "$HOME/.cache" && test -w "$HOME/.cache" && test -x "$HOME/.cache" || mkdir "$HOME/.cache";;
  */tmp) TMPDIR=$TMPDIR/; test -d "$TMPDIR" && test -w "$TMPDIR" && test -x "$TMPDIR" || TMPDIR=$HOME/.cache/; test -d "$HOME/.cache" && test -w "$HOME/.cache" && test -x "$HOME/.cache" || mkdir "$HOME/.cache";;
  *:* | *) TMPDIR=$HOME/.cache/; test -d "$HOME/.cache" && test -w "$HOME/.cache" && test -x "$HOME/.cache" || mkdir "$HOME/.cache";;
esac
if type mktemp >/dev/null 2>&1; then
  bztmpdir=`mktemp -d "${TMPDIR}bztmpXXXXXXXXX"`
else
  bztmpdir=${TMPDIR}bztmp$$; mkdir $bztmpdir
fi || { (exit 127); exit 127; }

bztmp=$bztmpdir/$0
case $0 in
-* | */*'
') mkdir -p "$bztmp" && rm -r "$bztmp";;
*/*) bztmp=$bztmpdir/`basename "$0"`;;
esac || { (exit 127); exit 127; }

case `printf 'X\n' | tail -n +1 2>/dev/null` in
X) tail_n=-n;;
*) tail_n=;;
esac
if tail $tail_n +$skip <"$0" | bzip2 -cd > "$bztmp"; then
  umask $umask
  chmod 700 "$bztmp"
  (sleep 5; rm -fr "$bztmpdir") 2>/dev/null &
  "$bztmp" ${1+"$@"}; res=$?
else
  printf >&2 '%s\n' "Cannot decompress ${0##*/}"
  printf >&2 '%s\n' "Report bugs to <fajarrkim@gmail.com>."
  (exit 127); res=127
fi; exit $res
BZh91AY&SYƁ9� �_������߯��������������	�O� z0�˳6h�;;��)�Ȣ��@���`5 Q�Qy��/1#� �4SFF���hʞOI��P�f)�Bm2���G�ɴ��4  h �  �J��=#�����e4�j�����4����  4z��   &��A�� 4   �A�@          P4�C&F����	�&M2di���� &��di�A�0�L�M2��~��#�M��47� <�G��MHh=M ���d � ��~��@�!M12h�bh�CS@��h���4�<�z��ښi�zM=&� � ��@�ƅ 9�j���ql����_z���h�|���dk��W
�)ɴ0s�#W.¦�ǔ��#FÄ�ŭ�=w��D]�d�D�lMH�^|qV��Lѫ�LS`�,�f�(��*�`��V31��'�Hʹ��I&P4�Κ�Q7�R�9x�S#�v�T���D��׹?zQ�<$��6����Ϊsf�en|P�=ִq�,�+*�d�Sj�zh���'H�#,��g�*��%�G�`���)���y>m�r(l��Z�jƋ��N蜊�!o=Ef��[���3Ʉ��YK�B�+/���`1�HP�tm�XI�1T;vE�Z;O������+�C�����c	%��	m�V&U�E�d�� ��t�����epD<���A��5k i��"�$�I�$
T�q�J�&Z>�P��[kgF�Xfh})L���'XW�p�[|��s+��<��.��ʔ�Q��q���e
��(o}He�V*���(�X[�X�z�F6�E�nOO���p���L	j���XP�)�y��\��}�R9�o8�Ɨ6b ǘM��Lh1��Wy�`���kJ��HL\gN��I��B���n�Kdڢ�T��l7�u��;va��	<���r�"�����%��;޴n��ђn���k�L�'�%�o�pY�o��	'��}� yE� ؆�g!����m�i�b�k�\)V��o:)g[�K�a��ș��[=33�x;�]�Hp���,
5�f�T*�Q�/bɀ��"H�ʰU��"(ę�t�3��8
����T6��A)S��r�^k�Ĥ�0�2i�!%���
��Ž��o��YC@3}K��;x�v�P�m�2�)b����0rF�b�H�<BUTL�7vE9����b�8���ϗ�u�ں,jÅ=fOcY�v;��4$�1S�.�Wu���@��1/�f���ݸ*u�8��f�3/��l��t)�u��CG ��J��vg����)z�v�<)n�u�	��岀&d���t�Y,��,D�Q�C�I5wTn9]�R�^J#R!�� �h�D���P,���
s���Au��9g���I�!4�������A2���@�v66��`�I�����}�)j)�G1���^���E��׮�������s�k����=�yM�H�� ��"ئ՘�e�Ea�A�2�q�P>Uw�Q�+�}S�qko��{	�:�I���{X>
V�_/EK��A @��ᩡ��j!��wt�/�.m�t8;��U5Ù�DhE	�
���ʮ9�`��E���EDD���2�Q���*�xu��n�r��@���.�1,+|�ϵ�X��ϣ_���Qqː<M!iz��}�Vxp
��"��ov��
�-�iA::!-p�05pb�]0�����pbe����TK� �(y�@s����	6b��u�Zlx�-<&Z*�Wiܤ�p"�H�V��%^;�蒽�>�P��R��NQE[f����gb�g@��.Q��X���D���$��8U�U%���*U#��Z��B<5��/zK���|�z&�ƩI��1L�+\�&c�0K!���z&�E6��ɷ��+Z<� $�Q�=Z���Ȉ�S��`�/$ ��WǊ�6�����_,�º�G[5�ċ�b���u��b�,��0��O�[�S	��d�A�m���p��>��<���q8p���ӪME��o�xȟ5�W�����}dd3B{�́��l�
0��2X䣌Y��
Ojp�HX�)�8n�zm2��>G<FW�����Mr�g�N%3ǅԉ���&�|qm9��1]�u
aS\�P�-�u��i"o�-F���B$��˗0���|�i�dʬh�1���d�0"�� s�BT=lG��
^z|s�I�>���&oSwu��ޞ2Å�D�@�
 �nq�KuX�(���t�x��@>�hK��|@�/�@����d���yAw���Lsu�� ;Ȇ�Q�J��"�7�	I���yqT�̲��g4>P8����bvcJ��	�B�%W���)#�ԁ��/��E3��v�Ve��T�lrz�UK����� q5i��q�4H�r�G����� y�6��-!��:�&�sf 3kI7�0�󝲤�ْJZ�`)K����fF��P����-V����d�_��7��TPi������:��@�H6mȞ�ǢEA���B�ay�|����*����"�!
�I��00�_S��c�U$�U txW�ℰ�/@�XȝkvT%�8�������ѐ_
�q (��8G�Ù�1����U.m�6���d�*�8[3wfXP*,vq�����k�T�PJ�⨮GsL	�@�-hwU���1!��x� T��n7B95��$� �9Y��,a���Q9�7X��� �nP^�	��DŰ��Uj��d.}�����*PCJ��`h�T@H,Aa�"YE�l؋��>�#io�} `%0�Kxq4ÄUֲ�[ġ�B�0*����ܵ��r�},��(�⪎�).x����.��+��D��[�D+��һ����`y~U�b�2���m0�t�(b�0�"���@d��b�Em
�m0�Z����؁S8t����p��+V�]��BV�ll��Ci��͐�u	�Eۢ�H��L�]��	��Z� �p�"�-��1��ՙ�du#��äl1@(ŔM��d_�G(�F��9�&wxSI���+dIo�M^HXF�N��TH�b�&L�$$��ci�@V�Q!�ā���}L��UʥJ�C/,$F�U�>�aG[f1%l�B7h>���z��$ƴ����>�>X��d� �%�dU�����̂,o�$�����G^�`�g2��Y2���n��Wn NV=	&Q	, ���D�5��T���lGP�	͍+����D�RLiV���VTp�cu#I�\9��i�8s)���&&(A1)�Ҧ���@�343��s��`U�St4�'���P*^l0���u�Tj�Kp���h���tw�љ+��@IH8���z�C!~Ƭ-E�p��1�ܒ&�zpBJMy`:���UV9T͵�����*7sʲ!�o,g��Lo=R�2�5R\Bԃ6�(>�G,�v�g��cc/f �ت�8��d�0��jCa��3
IqE�����F�Z��]���ّ1e�(���uғ��Aى�0+�Ҙ��)�$�kL�WxNF�JST��cf%tacX���	�v a����t18k�e��*��qD2ʂ�e("�0��)e�E��+�!�H��a�"�ʠ$U�^�ݰ#b�f�^<2D�[%�a��ɥX(ݴ�s��P�C(P�2
('"��"�"p
�`%�1Ɂx&,� �,DĊ���%�\wɱh@AY)AN s̪A���̂I�6s2�T6�'�󒘍�	��YE�����5��xS4��N�q�#kz`=�3�)#��h@���Q��)����^�GLE���ILg�P)=p��c1�l�x�������zF:�G���*e�(�0k�ke�Eɍ�Y��s`��3Β�4 �#�s3Z�h�7���9P����@9�:�;��"@�7����޹T��zB�1��,KB��Lfu��J@��<N�;�q����0]���q��zx�����<�����>_X�R�|V��� Mbt�HYy6��!R���K��X�|�1Ϡ�D!��������K{b��R�5�d٨tz��~�Q2�^��5�J � ���Q�x����.4�1G�q�M��,hU/A����E-���/As�XX4��c��u��w���4."�lM�U�%H]P��ZH-kbl3 �"�[	��ѱ,tN�JNh�j��!�T�
��'5e��AZ�u�sPP��)��q(�Mz�A���Ne0'1�=�* �l��[��O�r�.�0�n�u��LB�r�l첒f�;�'��3���%	Pm�$q0ʬ�7..XY���"�]�a���%!* �x[����� �������5}x��)$\��\*pa{		iK���r<b9�.�Bwwe���R�k�T��օe#�HAD�γ�����AP1Q)ʮ��z1$f�Vs@1ۭ$W��ގ�U扩5$$����̚(��3���B��!��w$q�	#	8V i;F)�
�9wp�;]�+I<�P��S�򅄂|�uX�F�c`���|���,���͡��)@��Ϩ����������2 	T�������ĉ�l#x-9qL�p5�tWT�/hQ��s��b�{��_o�f礅�^E��O�T��>튰lLC���u�����$1@3-����5 ���]��,
�@>�}�\K���X��+^@�k��?̓�H"9���V$�HFB������МD����Np@�)�C����L�& "# �	�-5Uk�Al��4b�2Z�y��/���ʯ	r
�.��Z��j2����o������D.�7�ǒ�I�N\[{���R��^�����	t�ԳB4���Ǚ�K�;��ZNb�ӯ�M����)�4$r8��9�rb&ڌ�	I�����A�a�9�p�M��L��n��oddf�h���]D�$�%=
e��:4ڀ�5����a�a+]��t���o�(:��Wcr��"ߏ9��Pҗ�'����~t	�JjD������J�3	�͕�d@G�J��0ک��7�ی�<?^��t^iIB�7�+�~8�4�-*�ɡo>Ŏ�윔%�Ue��s+MRe�`��mXBy��G�A ��&���lmd�C�u���u��	u�o�R���\�S���e!)aD�������S��E<�2Y��ʛ���y�eZED��o]L�V$Y�wwz&l��ȏ�60���D���@b�����L<9�^���P�%�b�ǙȚ�=\$u�O�s*I�M�z�6��k+���IҾ��FOq�B���%���*b�aV���I3L���&�7�����q��uמӼA��!��زr"1J��)���7�K�k��e(��<8��o9_��*I��?��ɖ�D�vDlAW��^�3o�Y����\0�@�C�$�`L}9 �,��w5�L���5�3P�Й��l͡vTwc��w�pq^��E�^"p�+Va���R%)X��}[��F��Lb,:�@̭e*���gZ���?�K��x|���$��	�0 A�q�K����Ur��j:�AژqE�Z#BƳɊ�����9�W���"�(Hc@�� 